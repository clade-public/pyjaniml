# CHANGELOG

## Unreleased

## 1.0.0 (2023-10-19)

- add documentation for opus file conversion
- release verion 1.0.0

## 0.11.0 (2023-10-18)

- add conversion of opus files

## 0.1.0 (2022-03-31)

- Dropped support for Python 3.6 and 3.7.

- Internal refactoring and code layout (enforce black, flake8, isort, and mypy).

## 0.0.11 (2021-02-25)

- Internal changes

## 0.0.10 (2021-02-25)

- Internal changes

## 0.0.9 (2021-01-22)

- Internal changes

## 0.0.8 (2021-01-22)

- Internal changes

## 0.0.7 (2021-01-22)

- Internal changes

## 0.0.6 (2020-11-17)

- Internal changes

## 0.0.5 (2020-11-17)

- Internal changes

## 0.0.4 (2020-11-17)

- Internal changes

## 0.0.4 (2020-11-17)

- Internal changes

## 0.0.3 (2020-11-17)

- Internal changes

## 0.0.2 (2020-11-17)

- Internal changes

## 0.0.1 (2020-11-17)

- Initial release
